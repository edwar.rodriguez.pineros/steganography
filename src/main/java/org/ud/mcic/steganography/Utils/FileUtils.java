package org.ud.mcic.steganography.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {
	public static void writeToFile(String path, byte[] fileContent) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(fileContent);
        fos.flush();
        fos.close();
    }
    
    public static void writeToFile(String path, String fileContent) throws IOException {
        File f = new File(path);
        f.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(f);
        fos.write(fileContent.getBytes());
        fos.flush();
        fos.close();
    }
    
    public static String readFileContent(String path) throws IOException {
    	byte[] encoded = Files.readAllBytes(Paths.get(path));
    	return new String(encoded,  StandardCharsets.UTF_8);
    }
    
    public static byte[] readFileBytes(String path) throws IOException {
    	return Files.readAllBytes(Paths.get(path));
    }
}
