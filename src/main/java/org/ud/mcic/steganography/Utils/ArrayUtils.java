package org.ud.mcic.steganography.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ArrayUtils {

	public static byte[] concatenateArrays(byte[] a, byte[] b) {
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}
    
    public static byte[] concatenateHashPasswordWithSaltToMessage(String pass, byte[] salt, String message) throws NoSuchAlgorithmException {
        byte[] password = pass.getBytes();
        byte[] saltedPass = ArrayUtils.concatenateArrays(password, salt);
        
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        byte[] saltedPassHash = md.digest(saltedPass);  
        
        byte[] messageBytes = message.getBytes();
        byte[] messageBytesWithSaltedPassHash = ArrayUtils.concatenateArrays(messageBytes, saltedPassHash);
        return messageBytesWithSaltedPassHash;
    }
    
	public static byte[] concatenateArrayAndHash(String pass, byte[] salt) throws NoSuchAlgorithmException {
        byte[] password = pass.getBytes();
        byte[] saltedPass = ArrayUtils.concatenateArrays(password, salt);
        
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");  
        byte[] saltedPassHash = md.digest(saltedPass);
        return saltedPassHash;
	}
    
}
