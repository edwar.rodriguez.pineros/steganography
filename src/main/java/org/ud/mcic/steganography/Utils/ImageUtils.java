package org.ud.mcic.steganography.Utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.BitSet;

import javax.imageio.ImageIO;

public class ImageUtils {
	
	
	public static void hideTextInImageLSB(String message, String imagePath, String imageModifiedPath) throws IOException {
		BufferedImage img = null;
		BufferedImage img2 = null;
		
		img = ImageIO.read(new File(imagePath));
		
		int width = img.getWidth(); //ancho
		int height = img.getHeight(); //alto
		
		int fillingSize = (width*height/8) - message.length();
		
		System.out.println();
		System.out.println("Image Width in pixels: " + width);
		System.out.println("Image Height in pixels: " + height);
		System.out.println("Image Capacity in bytes: " + (width*height/8));
		System.out.println("Message length in bytes: " + message.length());
		System.out.println("Filling Size: " + fillingSize);		
		System.out.println();
		
		//*********************************** DATA HANDLING AND FILLING ********************************
		
        StringBuilder sb = new StringBuilder();
        sb.append("${START}$");
        sb.append(message);
        sb.append("${END}$");
        sb.append(TextUtils.getAlphaNumericString(fillingSize));
        
        message = sb.toString();
        
        //System.out.println(message);
		
		byte[] messageBytes = message.getBytes();
		
		System.out.println("Filled Message length in bytes: " + messageBytes.length);
		System.out.println();
		
		//*********************************** HIDING MESSAGE IN IMAGE LSB - BLUE ARRAY ********************************
		
		img2 = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		int msgIdx = 0;					

		
		for(int i = 0 ; i < width - 1 ; i++) {
			for(int j = 0 ; j < height - 1 ; j++) {
				
				int rgb_ij = img.getRGB(i, j);
				
				if(msgIdx <= (messageBytes.length * 8 - 1)) {
					
					int red_ij = (rgb_ij >> 16) & 0xFF;
					int green_ij = (rgb_ij >> 8) & 0xFF;
					int blue_ij = rgb_ij & 0xFF;
					
					int bit = getBit(messageBytes, msgIdx);
					
					int blue_ij_st = (blue_ij & 0xFE) + bit;
					
					//int rgb_ij_st = (rgb_ij & 0XFFFFFF00) & blue_ij_st;
					
					Color color = new Color(red_ij, green_ij, blue_ij_st); // Color white
					int rgb_ij_st = color.getRGB();
					
					img2.setRGB(i, j, rgb_ij_st);
					
					//System.out.println("rgb_ij " + rgb_ij + " blue_ij " + blue_ij + ", bit " + bit + ", blue_ij_st " + blue_ij_st + ", rgb_ij_st " + rgb_ij_st );
					
					msgIdx++;
					
				} else {
					img2.setRGB(i, j, rgb_ij);
				}
					
			}
		}
		
	    File outputfile = new File(imageModifiedPath);
	    ImageIO.write(img2, "bmp", outputfile);
	}

	
	public static byte[] getHidenTextInImageLSB(String imagePath) throws IOException {
		
		BufferedImage img2Read = null;
		
		img2Read = ImageIO.read(new File(imagePath));
		
		int width = img2Read.getWidth(); //ancho
		int height = img2Read.getHeight(); //alto
		
	    BitSet bits = new BitSet(width * height);
	    
		//*********************************** READING MESSAGE IN IMAGE LSB - BLUE ARRAY ********************************
	    int msgIdxRead = 0;
	    
		for(int i = 0 ; i < width - 1 ; i++) {
			for(int j = 0 ; j < height - 1 ; j++) {
				
				int rgb_ij = img2Read.getRGB(i, j);
				if(msgIdxRead <= (width * height - 1)) {
					
					int blue_ij = rgb_ij & 0xFF;
					
					int bit = blue_ij & 0X01;
					
					if(bit == 1) {
						bits.set(msgIdxRead);
					}
					
					//System.out.println("rgb_ij " + rgb_ij + " blue_ij " + blue_ij + ", bit " + bit );
					//System.out.print(bit);
					msgIdxRead++;
				}
			}
		}

		byte[] messageBytesRead = toByteArray(bits);
		
		return messageBytesRead;
	}
	
	private static int getBit(byte[] data, int pos) {
	      int posByte = pos/8;
	      int posBit = pos%8;
	      byte valByte = data[posByte]; 
	      int valInt = valByte>>(8-(posBit+1)) & 0x0001;
	      return valInt;
	}
	
	private static byte[] toByteArray(BitSet bits) {
		byte[] bytes = new byte[ (bits.length() / 8) + 1 ];
		
		int i = 0;
		while(i < bits.length()) {
			String byteStr = "";
		    for (int j = 0; j < 8; j++) {
		    	byteStr = byteStr + (bits.get(i) ? 1 : 0);
		    	i++;
		    }
		    bytes[(i/8 - 1)] = Byte.parseByte(byteStr,2);
		}
	    return bytes;
	}
}
