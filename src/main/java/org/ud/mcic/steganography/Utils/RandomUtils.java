package org.ud.mcic.steganography.Utils;

import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.time.LocalTime;

public class RandomUtils {
		SecureRandom random;
		
		public RandomUtils() {
			byte[] seed =  ByteBuffer.allocate(16).putInt(LocalTime.now().getNano()).array();
	        random = new SecureRandom();
	        random.setSeed(seed);
		}
		
		public byte[] getNextSalt(int passLength) {
	        byte bytes[] = new byte[16 - passLength];
	        random.nextBytes(bytes);
	        return bytes;
	    }
}
