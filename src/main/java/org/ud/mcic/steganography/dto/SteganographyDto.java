package org.ud.mcic.steganography.dto;

import java.io.Serializable;
import java.util.Arrays;

public class SteganographyDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6402830521935397241L;
	
	private String encryptedMessage;
	private byte[] encryptedPass;
	private byte[] initVector;
	private byte[] salt;
	
	
	public String getEncryptedMessage() {
		return encryptedMessage;
	}
	public void setEncryptedMessage(String encryptedMessage) {
		this.encryptedMessage = encryptedMessage;
	}
	public byte[] getEncryptedPass() {
		return encryptedPass;
	}
	public void setEncryptedPass(byte[] encryptedPass) {
		this.encryptedPass = encryptedPass;
	}
	public byte[] getInitVector() {
		return initVector;
	}
	public void setInitVector(byte[] initVector) {
		this.initVector = initVector;
	}
	public byte[] getSalt() {
		return salt;
	}
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
	

	@Override
	public String toString() {
		return "SteganographyDto [encryptedMessage=" + encryptedMessage + ", encryptedPass=" + encryptedPass
				+ ", initVector=" + Arrays.toString(initVector) + ", salt=" + salt + "]";
	}
	
}

