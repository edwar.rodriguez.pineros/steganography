	package org.ud.mcic.steganography;

import java.nio.file.Paths;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.ud.mcic.steganography.Utils.AESUtils;
import org.ud.mcic.steganography.Utils.ArrayUtils;
import org.ud.mcic.steganography.Utils.FileUtils;
import org.ud.mcic.steganography.Utils.ImageUtils;
import org.ud.mcic.steganography.Utils.RSAUtils;
import org.ud.mcic.steganography.Utils.RandomUtils;
import org.ud.mcic.steganography.dto.SteganographyDto;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Steganography
 */
public class App {
	
	private static String path = "C:\\Users\\edwar.rodriguez\\Documents\\Blockchain\\steganography";
	
	//private static String file2Read = "BigAndLittleEndian";
	//private static String file2Read = "Genesis";
	private static String file2Read = "OldTestament";
	
	//private static String image2Read = "Wallpaper";
	//private static String image2Read = "Apple";
	private static String image2Read = "Planet";
	
	private static boolean logContent = false;
	//private static boolean logContent = true;
	
	public static void main(String[] args) {
		System.out.println("Steganography");

		try {
			
			StopWatch watch = new StopWatch();
			watch.start();
	        
			String clearText = FileUtils.readFileContent(Paths.get(path, file2Read + ".txt").toString());;
			
			System.out.println("clearText Message length in bytes: " + clearText.getBytes().length);
			System.out.println("clearText Message length in bytes: " + clearText.length());
	        
	        if(logContent) {
		        System.out.println("\nClear text to encrypt:");
		        System.out.println(clearText);
	        }
			
			//*********************************** RSA ENCRYPTION SECTION AND KEY EXPANSION ****************
			
			RSAUtils.generateKeyPair(path);
	        
	        String clearPass = "pass";
	        
	        RandomUtils randomUtils = new RandomUtils(); 
	        byte[] salt = randomUtils.getNextSalt(clearPass.length());
	        byte[] saltedPassHash = ArrayUtils.concatenateArrayAndHash(clearPass, salt);
	        
	        PublicKey publicKeyFromFile = RSAUtils.getPublicKeyFromFile(Paths.get(path, "RSA/publicKey.pem").toString());
	        
	        byte[] keyEncrypted = RSAUtils.encryptRSA(saltedPassHash, publicKeyFromFile);
	        
	        //*********************************** AES ENCRYPTION SECTION **********************************
	        
	        IvParameterSpec ivspec = AESUtils.generateIv();
	        
	        String encryptedString = AESUtils.encryptAES(clearText, ivspec, saltedPassHash);
	        
	        if(logContent) {
		        System.out.println("\nEncrypted String with AES: ");
		        System.out.println(encryptedString);
	        }
			
	        //*********************************** DATA HANDLING **********************************
	        
	        SteganographyDto dto = new SteganographyDto();
	        dto.setEncryptedMessage(encryptedString);
	        dto.setEncryptedPass(keyEncrypted);
	        dto.setInitVector(ivspec.getIV());
	        dto.setSalt(salt);
	        
	        ObjectMapper om = new ObjectMapper();
	        String message = om.writeValueAsString(dto);

	        //*********************************** STEGANOGRAPHY ********************************
	        
			ImageUtils.hideTextInImageLSB(message,
					Paths.get(path, image2Read + ".bmp").toString(),
					Paths.get(path, image2Read + "2.bmp").toString());
			
			//*********************************** STEGANOGRAPHY READING ************************
			
			byte[] messageBytesRead = ImageUtils.getHidenTextInImageLSB(Paths.get(path, image2Read + "2.bmp").toString());
			
			FileUtils.writeToFile(Paths.get(path, "DataReadedFromImage.txt").toString(), messageBytesRead);
			
			//*********************************** DATA INTERPRETATION **************************
			
			String dataReadedFromFile = FileUtils.readFileContent(Paths.get(path, "DataReadedFromImage.txt").toString());;
			
			String steganographyStrRead = StringUtils.substringBetween(dataReadedFromFile, "${START}$", "${END}$");
			
			if(logContent) {
				System.out.println("steganographyStrRead: " + steganographyStrRead);
			}
			
	        ObjectMapper om2 = new ObjectMapper();
	        SteganographyDto steganographyDtoRead = om2.readValue(steganographyStrRead, SteganographyDto.class);	        
	        
	        //*********************************** RSA DECRYPTION AND PASSWORD VERIFICATION SECTION **********************************
	        
	        PrivateKey privateKeyFromFile = RSAUtils.getPrivateKeyFromFile(Paths.get(path, "RSA/privateKey.pem").toString());
	        
	        byte[] keyDecrypted = RSAUtils.decryptRSAByte(steganographyDtoRead.getEncryptedPass(), privateKeyFromFile);
	        
	        String clearPass2Decrypt = "pass";
	        byte[] saltedPassHash2Decrypt = ArrayUtils.concatenateArrayAndHash(clearPass2Decrypt, steganographyDtoRead.getSalt());
	        
	        if(!(new String(saltedPassHash2Decrypt).equals(new String(keyDecrypted))))
	        {
	        	throw new Exception("Wrong Password");
	        }
	        
	        //*********************************** AES DECRYPTION SECTION **********************************
	        
	        String decryptedString = AESUtils.decryptAES(steganographyDtoRead.getEncryptedMessage(),
	        		new IvParameterSpec(steganographyDtoRead.getInitVector()),
	        		keyDecrypted);
	        
	        if(logContent) {
		        System.out.println("\nText after decryption:");
		        System.out.println(decryptedString);
	        }
	        
	        FileUtils.writeToFile(Paths.get(path, file2Read + "_Read.txt").toString(), decryptedString);
	        
	        watch.stop();
	        System.out.println("Time Elapsed: " + watch.getTime());
	        
		} catch (Exception e) {
			e.printStackTrace();
		}

	}	
}
